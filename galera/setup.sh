#!/bin/bash

cat <<EOF > /etc/yum.repos.d/mariadb.repo
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF

yum makecache --disablerepo='*' --enablerepo='mariadb'

yum install MariaDB-server MariaDB-client rsync policycoreutils-python

systemctl enable mariadb

systemctl start mariadb

echo -n "Enter temporary password"
read -s GENERATED_PASSWORD

echo -n "Enter new password"
read -s NEW_PASSWORD

echo "Changing root password"
mysql -uroot -p${GENERATED_PASSWORD} -e "set password = password(\"${NEW_PASSWORD}\");"

systemctl stop mariadb

echo "Enter Node 1 Priv IP"
read NODE1PRIV

echo "Enter Node 2 Priv IP"
read NODE2PRIV

echo "Enter Node 3 Priv IP"
read NODE3PRIV

echo "Current NODE PRIV IP"
read NODECURRENTPRIV

echo "Current Node Name:(wsrep_node_name e.g node1)"
read NODENAME

cat <<EOF > /etc/my.cnf.d/galera.cnf

[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib64/galera-4/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="dbcluster"
wsrep_cluster_address="gcomm://${NODE1PRIV},${NODE2PRIV},${NODE3PRIV}"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="${NODECURRENTPRIV}"
wsrep_node_name="${NODENAME}"
EOF