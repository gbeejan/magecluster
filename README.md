**How to setup a mage cluster on stack VARNISH, NGINX, PHP-FPM, REDIS, MARIADB GALERA CLUSTER**

This repository will allow you to create a VARNISH, NGINX stack with PHP-FPM, MariaDB Galera Cluster, Redis for a Magento 2 Commerce.

Refs: https://devdocs.magento.com/guides/v2.3/install-gde/prereq/nginx.html

---

# GALERA Cluster Setup


The installation of a 3 node cluster can be set-up by running the **galera/setup.sh** file on each **server**.

It will:

1. Install mariadb-server as a single node
2. Provide a temporary password on screen through installation that will need to be re-entered through the script
3. Ask you for the new root password
4. Ask you for the nodes IP addresses
5. Replace the IP addresses in the wsrep configuration in /etc/my.cnf
6. Restart mariadb

## Execute Setup File

```bash
chmod +x setup.sh
./setup.sh
```

## Start the databases

**Always start the first node as below**

```bash
root@node1 $ galera_new_cluster
root@node2 $ systemctl start mariadb
root@node3 $ systemctl start mariadb
```

** If whole cluster is stopped, verify /var/lib/mysql/grastate.dat on the other nodes safe_to_bootstrap: 0, and change safe_to_bootstrap to 1 on the first node **

```bash
#Verify cluster status
#run on master node
mysql -e "SHOW STATUS LIKE 'wsrep_cluster_size'";
```

## Create Users on Galera
```SQL
MYSQL> use mysql;
MYSQL> CREATE USER haproxy@'%';
MYSQL> CREATE DATABASE magentodb1;
MYSQL> CREATE USER magentous1@'localhost' identified by magentous1;
MYSQL> GRANT all privileges on magentodb1.* to magentous1@'localhost';
MYSQL> GRANT super on *.* to magentous1@'localhost';
```

---


# NGINX PHP-FPM SETUP

## Install Nginx

```bash
yum -y install epel-release
yum -y install nginx
systemctl start nginx
systemctl enable nginx
```

## Install and configure php-fpm

```bash
yum install -y http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-15.ius.centos7.noarch.rpm
rm -f /etc/yum.repos.d/ius.repo
wget -O /etc/yum.repos.d/ius.repo https://repo.ius.io/ius-7.repo
yum -y update
yum -y install php72u php72u-fpm php72u-pdo php72u-mysqlnd php72u-opcache php72u-xml php72u-gd php72u-devel php72u-mysql php72u-intl php72u-mbstring php72u-bcmath php72u-json php72u-iconv php72u-soap

```

Verify php version
```bash
php -v
```
> PHP 7.2.22 (cli) (built: Aug 30 2019 15:12:58) ( NTS )  
Copyright (c) 1997-2018 The PHP Group  
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies  
    with Zend OPcache v7.2.22, Copyright (c) 1999-2018, by Zend Technologies

Open the **/etc/php.ini** file in an editor.

Uncomment the cgi.fix_pathinfo line and change the value to 0.

Edit the file to match the following lines:

> memory_limit = 2G  
 max_execution_time = 1800  
 zlib.output_compression = On

Uncomment the session path directory and set the path:

> session.save_path = "/var/lib/php/session"

Open /etc/php-fpm.d/www.conf in an editor.

Edit the file to match the following lines:
> user = nginx  
 group = nginx  
 listen = /run/php-fpm/php-fpm.sock  
 listen.owner = nginx  
 listen.group = nginx  
 listen.mode = 0660

Uncomment the environment lines:

> env[HOSTNAME] = $HOSTNAME  
 env[PATH] = /usr/local/bin:/usr/bin:/bin  
 env[TMP] = /tmp  
 env[TMPDIR] = /tmp  
 env[TEMP] = /tmp


Create php directories
```bash
mkdir -p /var/lib/php/session/
chown -R nginx: /var/lib/php/session
mkdir -p /run/php-fpm/
chown -R nginx: /run/php-fpm
```

Start PHP FPM
```bash
systemctl start php-fpm
systemctl enable php-fpm
ss -pl | grep php-fpm.sock
```


## HaProxy for MariaDB TCP Load Balancing

```bash
yum install haproxy
```

Replace ***/etc/haproxy/haproxy.cfg*** from ***haproxy/haproxy.cfg***

```bash
systemctl enable haproxy
systemctl start haproxy
yum install mariadb
#TEST mysql connectivity via haproxy
mysql -u haproxy -h 127.0.0.1
```

---

# MAGENTO 2 Commerce SETUP

Magento Installation

```bash
yum install unzip php72u-zip php72u-cli wget
cd /opt
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer

mkdir -p /var/www/magento
composer create-project --repository=https://repo.magento.com/ magento/project-enterprise-edition /var/www/magento

cd /var/www/magento/
find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +
chown -R nginx: .


bin/magento setup:install \
 --base-url=http://magento.redbox.local \
 --db-host=127.0.0.1 \
 --db-name=magentodb1 \
 --db-user=magentous1 \
 --db-password=magentous1 \
 --backend-frontname=admin \
 --admin-firstname=admin \
 --admin-lastname=admin \
 --admin-email=admin@admin.com \
 --admin-user=admin \
 --admin-password=admin123 \
 --language=en_US \
 --currency=USD \
 --timezone=America/Chicago \
 --use-rewrites=1
```

Switch to developer mode:

```bash
cd /var/www/html/magento2/bin
./magento deploy:mode:set developer
```

Install Magento Cron

```bash
su - nginx
php bin/magento cron:install 
```


## Configure nginx for phpfpm

```bash
vim /etc/nginx/conf.d/magento.conf
```

> upstream fastcgi_backend {  
   server  unix:/run/php-fpm/php-fpm.sock;  
 }  
  
> server {  
   listen 80;  
   server_name magento.redbox.local;  
   set $MAGE_ROOT /var/www/magento;  
   include /var/www/magento/nginx.conf.sample;  
 }


```bash
#verify nginx syntax
nginx -t

#Start nginx
systemctl restart nginx
```

check documentation https://devdocs.magento.com/guides/v2.3/install-gde/prereq/nginx.html if selinux and firewalld required

Any errors see /var/www/magento/var/log/debug.log


# VARNISH SETUP

## Install Varnish

```bash
yum -y install epel-release
yum -y install varnish
systemctl start varnish
systemctl enable varnish
```

## Configure Varnish

```bash
vi /etc/varnish/default.vcl
# backend default {
#    .host = "10.191.162.67";
#    .port = "80";
# }
#
# sub vcl_recv {
#    if (req.http.host == "magento.redbox.local") {
#        set req.backend_hint = default;
#    }
#    if (req.http.host == "control.redbox.local") {
#	set req.backend_hint = default;
#    }
#
# }


vi /etc/varnish/varnish.params 
#VARNISH_LISTEN_PORT=80

systemctl restart varnish
```

Add varnish IP as magento.redbox.local to test via curl

curl -IL magento.redbox.local

---


# REDIS SETUP

Redis setup here



# [ADD-ON] NGINX Image Resize

We will configure nginx dynamic image resizing. Let varnish handle the cache.

Test current speed via varnish cache only without resize. Use apache benchmark **ab** 

1000 requests using 100 concurrent connections:
```bash
ab -n 1000 -c 100 http://magento.redbox.local/media/catalog/product/cache/f38443242dcb2244f40e832ad0aed815/m/p/mp09-blue_back_1.jpg
```

Modify ***/etc/nginx/conf.d/magento.conf***





# Install N98-Magerun2 on nginx servers

```bash
cd /opt
wget https://files.magerun.net/n98-magerun2.phar
chmod +x ./n98-magerun2.phar
./n98-magerun2.phar --version
cp ./n98-magerun2.phar /usr/local/bin/
cd /var/www/magento
n98-magerun2.phar  store:list
#+----+------------+----------+--------------------+---------+------------+-----------+
#| ID | Website ID | Group ID | Name               | Code    | Sort Order | Is Active |
#+----+------------+----------+--------------------+---------+------------+-----------+
#| 0  | 0          | 0        | Admin              | admin   | 0          | 1         |
#| 1  | 1          | 1        | Default Store View | default | 0          | 1         |
#+----+------------+----------+--------------------+---------+------------+-----------+
```